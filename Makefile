stack_name=phoenix-corporate
profile=phoenix
region=eu-west-1
deploy_bucket=phoenix-watchlist-deploy
version=1.0.1
codeupload_path=s3://$(deploy_bucket)/phoenix-ownership/$(version)
codeVersion=1.0.1



deploy: upload
	aws cloudformation deploy \
    --template-file stack.yaml \
    --stack-name $(stack_name) \
	--parameter-overrides Version=$(version) CodeVersion=$(codeVersion) DeployBucket=$(deploy_bucket) \
    --capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM \
	--profile $(profile) \
	--region $(region)


upload:
	aws s3 sync ./ $(codeupload_path) \
	--exclude "*." \
	--include "*.yaml" \
	--profile $(profile)
